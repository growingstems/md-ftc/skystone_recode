package org.firstinspires.ftc.teamcode.autonomous;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;

import org.firstinspires.ftc.teamcode.implementations.Robot12888;

@Autonomous(name = "Example Auto")
public class AutoMain extends LinearOpMode {
    private Robot12888 robot = new Robot12888();

    @Override
    public void runOpMode() {
        robot.init(hardwareMap);

        waitForStart();

        robot.driveTrain.drive(3.0);
        robot.driveTrain.turn(45.0, 1.0);
        robot.driveTrain.strafe(true, 2.0);
    }
}
