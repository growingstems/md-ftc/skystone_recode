package org.firstinspires.ftc.teamcode.implementations;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.teamcode.interfaces.RetractableLift;

public class RetractableLiftImplement implements RetractableLift {
    private CRServo lifting;
    private CRServo bendServo;
    private CRServo gripServo;
    private boolean  isGripping = false;
    @Override
    public void init(HardwareMap hardwareMap) {
        bendServo = hardwareMap.crservo.get("as");
        gripServo = hardwareMap.crservo.get("gs");
        lifting = hardwareMap.crservo.get("rb"); //Motor on Spark Mini
        bendServo.setDirection(DcMotorSimple.Direction.FORWARD);
        gripServo.setDirection(DcMotorSimple.Direction.REVERSE);
        lifting.setDirection(DcMotorSimple.Direction.REVERSE);
    }
    @Override
    public void close() {
        gripServo.setPower(-1);
        isGripping = true;
    }

    @Override
    public void open() {
        gripServo.setPower(1);
        isGripping = false;
    }

    @Override
    public void bendBack() {
        bendServo.setPower(-1);
    }

    @Override
    public void bendUp() {
        bendServo.setPower(1);
    }

    @Override
    public void stopBending() {
        bendServo.setPower(0);
    }

    @Override
    public void stopGripping() {
        if(!isGripping){
            gripServo.setPower(0);
        }
        else{
            gripServo.setPower(-0.1);
        }
    }

    @Override
    public void lift(double power) {
        lifting.setPower(power);
    }
}
