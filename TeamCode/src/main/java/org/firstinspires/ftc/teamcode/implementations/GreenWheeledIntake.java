package org.firstinspires.ftc.teamcode.implementations;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.teamcode.interfaces.HingedIntake;

public class GreenWheeledIntake implements HingedIntake {
    private CRServo leftWheel;
    private CRServo rightWheel;
    private CRServo liftingIntake;

    @Override
    public void intake(double power) {
        //set the powers of the servos
        leftWheel.setPower(power);
        rightWheel.setPower(power);
    }

    @Override
    public void init(HardwareMap hardwareMap) {
        //link the servos to the hardware map
        leftWheel = hardwareMap.crservo.get("lw");
        rightWheel = hardwareMap.crservo.get("rw");
        liftingIntake = hardwareMap.crservo.get("ls");

        // set the direction of the servos
        leftWheel.setDirection(DcMotorSimple.Direction.FORWARD);
        rightWheel.setDirection(DcMotorSimple.Direction.REVERSE);
        liftingIntake.setDirection(DcMotorSimple.Direction.REVERSE);
    }

    @Override
    public void rotate(double power) {
        //let the lift be able to turn
        liftingIntake.setPower(power);
    }
}
