package org.firstinspires.ftc.teamcode.interfaces;

import com.qualcomm.robotcore.hardware.HardwareMap;

public interface HeadingSource extends Subsystem {
    @Override
    default void init(HardwareMap hardwareMap) {}

    /**
     * Inform HeadingSource of our current known heading
     * @param heading Current heading of the robot, in degrees
     */
    void setHeading(double heading);

    /**
     * Retrieves current robot heading from the HeadingSource
     * @return Current heading of the robot, in degrees
     */
    double getHeading();
}
