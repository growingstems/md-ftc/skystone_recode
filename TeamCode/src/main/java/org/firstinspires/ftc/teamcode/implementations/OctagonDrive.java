package org.firstinspires.ftc.teamcode.implementations;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.NavUtil;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.teamcode.interfaces.HeadingSource;
import org.firstinspires.ftc.teamcode.interfaces.HolonomicDriveTrain;
import org.firstinspires.ftc.teamcode.interfaces.PositionSource;

public class OctagonDrive implements HolonomicDriveTrain, PositionSource, HeadingSource {
    // Turn rate at max speed
    private final double TURN_DEGREES_PER_SECOND = 180.0;
    // Drive rate at max speed
    private final double DRIVE_INCHES_PER_SECOND = 18.0;

    private DcMotor leftFront;
    private DcMotor rightFront;
    private DcMotor leftBack;
    private DcMotor rightBack;

    //TODO: Run test to find actual ratios
    private static final double EC_PER_CM = 0;
    private static final double EC_PER_DEGREE = 0;

    private Position positionOffset = new Position();
    private double headingOffset = 0;

    @Override
    public void init(HardwareMap hardwareMap) {
        leftFront = hardwareMap.dcMotor.get("lf");
        rightFront = hardwareMap.dcMotor.get("rf");
        leftBack = hardwareMap.dcMotor.get("lb");
        rightBack = hardwareMap.dcMotor.get("rb");

        leftFront.setDirection(DcMotor.Direction.REVERSE);
        rightFront.setDirection(DcMotor.Direction.FORWARD);
        leftBack.setDirection(DcMotor.Direction.REVERSE);
        rightBack.setDirection(DcMotor.Direction.FORWARD);

        leftFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightFront.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        leftBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
        rightBack.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }

    @Override
    public void setPosition(Position position) {
        positionOffset = new Position();
        Position currentPosition = getPosition();
        positionOffset = NavUtil.minus(currentPosition, position);
    }

    @Override
    public Position getPosition() {
        double averageEncoderY = (leftBack.getCurrentPosition() + leftFront.getCurrentPosition() + rightBack.getCurrentPosition() + rightFront.getCurrentPosition()) / 4.0;
        double averageEncoderX = (-leftBack.getCurrentPosition() + leftFront.getCurrentPosition() + rightBack.getCurrentPosition() - rightFront.getCurrentPosition()) / 4.0;
        Position position = new Position(DistanceUnit.CM, averageEncoderX / EC_PER_CM, averageEncoderY/EC_PER_CM, 0.0, System.nanoTime());
        return NavUtil.minus(position, positionOffset);
    }

    @Override
    public void setHeading(double heading) {
        headingOffset = 0;
        headingOffset = getHeading() - heading;
    }

    @Override
    public double getHeading() {
        double averageEncoderHeading = (leftBack.getCurrentPosition() + leftFront.getCurrentPosition() - rightBack.getCurrentPosition() - rightBack.getCurrentPosition()) / 4.0;
        return averageEncoderHeading/ EC_PER_DEGREE - headingOffset;
    }

    @Override
    public void setDrive(double drivePower, double turnPower, double strafePower) {
        leftFront.setPower(drivePower + turnPower + strafePower);
        rightFront.setPower(drivePower - turnPower - strafePower);
        leftBack.setPower(drivePower + turnPower - strafePower);
        rightBack.setPower(drivePower - turnPower + strafePower);
    }

    @Override
    public void turn(double degrees, double maxPower) {
        double waitTime = degrees / (TURN_DEGREES_PER_SECOND * maxPower);
        setDrive(0.0, maxPower, 0.0);
        sleep(Math.round(waitTime * 1000));
        setDrive(0.0 , 0.0, 0.0);
    }

    @Override
    public void strafe(double inches, double maxPower) {
        double waitTime = inches / (DRIVE_INCHES_PER_SECOND * maxPower);
        setDrive(0.0, 0.0, maxPower);
        sleep(Math.round(waitTime * 1000));
        setDrive(0.0 , 0.0, 0.0);
    }

    @Override
    public void drive(double inches, double maxPower) {
        double waitTime = inches / (DRIVE_INCHES_PER_SECOND * maxPower);
        setDrive(maxPower, 0.0, 0.0);
        sleep(Math.round(waitTime * 1000));
        setDrive(0.0 , 0.0, 0.0);
    }

    private void sleep(long milliseconds) {
        long startTime = System.currentTimeMillis();
        while (System.currentTimeMillis() - startTime < milliseconds) {
            // TODO: Exit if program has stopped
        }
    }
}
