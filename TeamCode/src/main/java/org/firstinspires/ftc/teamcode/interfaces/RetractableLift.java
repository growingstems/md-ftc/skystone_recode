package org.firstinspires.ftc.teamcode.interfaces;

public interface RetractableLift extends Subsystem {
    void close();
    void open();
    void bendBack();
    void bendUp();
    void stopBending();
    void stopGripping();
    void lift(double power);
}
