package org.firstinspires.ftc.teamcode.implementations;

import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.hardware.bosch.JustLoggingAccelerationIntegrator;
import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.teamcode.interfaces.HeadingSource;

public class InternalImu implements HeadingSource {
    private BNO055IMU imu;
    private double offsetAngle = 0.0;

    @Override
    public void init(HardwareMap hardwareMap) {
        imu = hardwareMap.get(BNO055IMU.class, "imu");
        BNO055IMU.Parameters imuParameters = new BNO055IMU.Parameters();
        imuParameters.angleUnit = BNO055IMU.AngleUnit.DEGREES;
        imuParameters.accelUnit = BNO055IMU.AccelUnit.METERS_PERSEC_PERSEC;
        imuParameters.calibrationDataFile = "BNO055IMUCalibration.json"; // see the calibration sample opmode
        imuParameters.loggingEnabled = true;
        imuParameters.loggingTag = "IMU";
        imuParameters.accelerationIntegrationAlgorithm = new JustLoggingAccelerationIntegrator();
        imuParameters.temperatureUnit = BNO055IMU.TempUnit.FARENHEIT;

        imu.initialize(imuParameters);

    }

    @Override
    public void setHeading(double heading) {
        offsetAngle = 0.0;
        double sensorHeading = getHeading();
        offsetAngle = heading - sensorHeading;
    }

    @Override
    public double getHeading() {
        double heading = -imu.getAngularOrientation().firstAngle;
        heading += offsetAngle;
        if (heading > 180f)
            heading = ((heading + 180f) % 360f) - 180f;
        else if (heading < -180f)
            heading = 180f - ((180f - heading) % 360f);
        return heading;
    }
}
