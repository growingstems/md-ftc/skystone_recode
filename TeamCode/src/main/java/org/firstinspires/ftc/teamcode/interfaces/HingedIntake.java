package org.firstinspires.ftc.teamcode.interfaces;

public interface HingedIntake extends Subsystem, Intake {
    void rotate(double power);
}
