package org.firstinspires.ftc.teamcode.interfaces;

public interface Intake extends Subsystem {
    void intake(double power);
}
