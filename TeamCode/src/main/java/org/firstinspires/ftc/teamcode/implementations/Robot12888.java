package org.firstinspires.ftc.teamcode.implementations;

import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.teamcode.interfaces.HeadingSource;
import org.firstinspires.ftc.teamcode.interfaces.HingedIntake;
import org.firstinspires.ftc.teamcode.interfaces.HolonomicDriveTrain;
import org.firstinspires.ftc.teamcode.interfaces.PositionSource;
import org.firstinspires.ftc.teamcode.interfaces.RetractableLift;
import org.firstinspires.ftc.teamcode.interfaces.Subsystem;

public class Robot12888 implements Subsystem {
    public final HeadingSource headingSource = new InternalImu();
    public final PositionSource positionSource = null;
    public final HingedIntake greenWheelIntake = new GreenWheeledIntake();
    public final RetractableLift lift = new RetractableLiftImplement();
    public final HolonomicDriveTrain driveTrain = new OctagonDrive();

    @Override
    public void init(HardwareMap hardwareMap) {
        headingSource.init(hardwareMap);
        positionSource.init(hardwareMap);
        greenWheelIntake.init(hardwareMap);
        lift.init(hardwareMap);
        driveTrain.init(hardwareMap);
    }
}
