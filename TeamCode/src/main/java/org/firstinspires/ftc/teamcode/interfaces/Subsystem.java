package org.firstinspires.ftc.teamcode.interfaces;

import com.qualcomm.robotcore.hardware.HardwareMap;

public interface Subsystem {
    /**
     * Initialize the Subsystem using the provided HardwareMap
     * @param hardwareMap Map from OpMode to find hardware devices on robot
     */
    void init(HardwareMap hardwareMap);
}
