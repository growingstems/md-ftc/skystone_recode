package org.firstinspires.ftc.teamcode.interfaces;

import com.qualcomm.robotcore.hardware.HardwareMap;

import org.firstinspires.ftc.robotcore.external.navigation.Position;

public interface PositionSource extends Subsystem {
    @Override
    default void init(HardwareMap hardwareMap) {}

    /**
     * Inform PositionSource of our current known Position
     * @param position Current position of the robot, in inches
     */
    void setPosition(Position position);

    /**
     * Retrieves current robot position from the PositionSource
     * @return Current Position of the robot, in inches
     */
    Position getPosition();
}
