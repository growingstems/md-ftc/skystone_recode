package org.firstinspires.ftc.teamcode.teleop;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.implementations.Robot12888;

@TeleOp(name = "Teleop")
public class TeleopMain extends OpMode {
    private Robot12888 robot = new Robot12888();

    @Override
    public void init() {
        robot.init(hardwareMap);
    }

    @Override
    public void init_loop() {
        telemetry.addData("Heading", robot.headingSource.getHeading());
    }

    @Override
    public void start() {
        robot.headingSource.setHeading(0.0);
    }

    @Override
    public void loop() {
        // Lift
        if (gamepad2.dpad_up) {
            robot.lift.lift(1.0);
        } else if (gamepad2.dpad_down) {
            robot.lift.lift(-1.0);
        } else if (gamepad2.left_bumper) {
            robot.lift.lift(0.5);
        } else if (gamepad2.left_trigger > 0.1f) {
            robot.lift.lift(-0.25);
        } else {
            robot.lift.lift(0.0);
        }

        // Lift bending
        if (gamepad2.y) {
            robot.lift.bendBack();
        } else if (gamepad2.x) {
            robot.lift.bendUp();
        } else {
            robot.lift.stopBending();
        }

        // Lift Intake
        if (gamepad2.right_bumper) {
            robot.lift.close();
        } else if (gamepad2.right_trigger > 0.1f) {
            robot.lift.open();
        } else {
            robot.lift.stopGripping();
        }

        // Green-wheel intake
        if (gamepad1.y) {
            robot.greenWheelIntake.intake(1.0);
        } else if (gamepad1.a) {
            robot.greenWheelIntake.intake(-0.75);
        } else {
            robot.greenWheelIntake.intake(0.0);
        }

        // Green-wheel lift
        if (gamepad1.x) {
            robot.greenWheelIntake.rotate(1.0);
        } else if (gamepad1.b) {
            robot.greenWheelIntake.rotate(-1.0);
        } else {
            robot.greenWheelIntake.rotate(0.1);
        }
        telemetry.addData("Heading", robot.headingSource.getHeading());
    }
}
