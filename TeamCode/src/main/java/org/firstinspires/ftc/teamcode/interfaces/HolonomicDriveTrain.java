package org.firstinspires.ftc.teamcode.interfaces;

public interface HolonomicDriveTrain extends Subsystem {
    // Teleop-related methods
    void setDrive(double drivePower, double turnPower, double strafePower);
    default void setDrive(boolean driveForwards, double drivePower, boolean turnRight, double turnPower, boolean strafeRight, double strafePower) {
        if (!driveForwards)
            drivePower *= -1.0;
        if (!turnRight)
            turnPower *= -1.0;
        if (!strafeRight)
            strafePower *= -1.0;
        setDrive(drivePower, turnPower, strafePower);
    }

    // Autonomous-related methods
    void turn(double degrees, double maxPower);
    default void turn(double degrees) {
        turn(degrees, 1.0);
    }
    default void turn(boolean toRight, double degrees) {
        if (toRight)
            turn(degrees);
        else
            turn(-degrees);
    }

    void strafe(double inches, double maxPower);
    default void strafe(double inches) {
        strafe(inches, 1.0);
    }
    default void strafe(boolean toRight, double inches) {
        if (toRight)
            strafe(inches);
        else
            strafe(-inches);
    }

    void drive(double inches, double maxPower);
    default void drive(double inches) {
        drive(inches, 1.0);
    }
    default void drive(boolean toForwards, double inches) {
        if (toForwards)
            drive(inches);
        else
            drive(-inches);
    }
}
