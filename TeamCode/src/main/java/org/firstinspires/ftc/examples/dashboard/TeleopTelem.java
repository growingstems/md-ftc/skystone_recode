package org.firstinspires.ftc.examples.dashboard;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.dashboard.telemetry.MultipleTelemetry;
import com.acmerobotics.dashboard.telemetry.TelemetryPacket;
import com.qualcomm.robotcore.eventloop.opmode.Disabled;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.robotcore.external.navigation.Position;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.teamcode.Constants;
import org.firstinspires.ftc.teamcode.R;

@Disabled
@TeleOp(name="Ftc Dashboard", group="examples")
public class TeleopTelem extends OpMode {
    private final FtcDashboard dashboard = FtcDashboard.getInstance();
    private int configUpdateCounter = 0;

    private int lastRuntime = 0;

    @Config
    public static class ConfigData {
        public static Position position = new Position(DistanceUnit.INCH, 0.0, 0.0, 0.0, 0);
        public static double width = 18;
        public static double height = 18;
        public static double radius = 45.0;
    }

    @Config
    public static class OtherData {
        public static int changing = 0;
    }

    public TeleopTelem() {
        telemetry = new MultipleTelemetry(telemetry, dashboard.getTelemetry());
    }

    @Override
    public void init_loop() {
        TelemetryPacket packet = new TelemetryPacket();
        packet.put("Example init", 2.1f);
        dashboard.sendTelemetryPacket(packet);
    }

    @Override
    public void init() {
        msStuckDetectStop = 2500;
        VuforiaLocalizer.Parameters vuforiaParams = new VuforiaLocalizer.Parameters(R.id.cameraMonitorViewId);
        vuforiaParams.vuforiaLicenseKey = Constants.VUFORIA_LICENSE_KEY;
        vuforiaParams.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        VuforiaLocalizer vuforia = ClassFactory.getInstance().createVuforia(vuforiaParams);

        FtcDashboard.getInstance().startCameraStream(vuforia, 1);
    }

    public interface DoubleArraySupplier {
        double[] get(double w, double h, double angle);
    }

    @Override
    public void loop() {
        OtherData.changing++;

        double angle = getRuntime();
        double x = ConfigData.radius * Math.cos(angle) + ConfigData.position.x;
        double y = ConfigData.radius * Math.sin(angle) + ConfigData.position.y;
        drawRobot(x, y, angle);
        configUpdateCounter++;
        if (configUpdateCounter > 10) {
            dashboard.updateConfig();
            configUpdateCounter = 0;
        }

        int runtime = (int)(getRuntime()*1000);
        telemetry.addData("Time Between Loops, ms", runtime - lastRuntime);
        lastRuntime = runtime;
    }

    private void drawRobot(double x, double y, double angle){
        TelemetryPacket packet = new TelemetryPacket();
        DoubleArraySupplier xVals = (w, h, a) -> new double[]{ x + w*Math.cos(a) - h*Math.sin(a), x - w*Math.cos(a) - h*Math.sin(a), x - w*Math.cos(a) + h*Math.sin(a), x + w*Math.cos(a) + h*Math.sin(a) };
        DoubleArraySupplier yVals = (w, h, a) -> new double[]{ y + w*Math.sin(a) + h*Math.cos(a), y - w*Math.sin(a) + h*Math.cos(a), y - w*Math.sin(a) - h*Math.cos(a), y + w*Math.sin(a) - h*Math.cos(a) };
        packet.fieldOverlay().setFill("blue").fillPolygon(xVals.get(ConfigData.width/2.0, ConfigData.height/2.0, angle), yVals.get(ConfigData.width/2.0, ConfigData.height/2.0, angle));
        packet.fieldOverlay().setFill("red").fillPolygon(xVals.get(ConfigData.width/6.0, ConfigData.height/6.0, angle), yVals.get(ConfigData.width/6.0, ConfigData.height/6.0, angle));
        dashboard.sendTelemetryPacket(packet);
    }
}
